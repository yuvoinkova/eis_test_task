# RUN task_2(num, k) to get result

def check_num(num):
    # check if the input string in INVALID format (then True)
    try:
        for symbol in num:
            if int(symbol) not in range(10):
                raise ValueError('Incorrect num format!')
        return False
    except Exception:
        return True


def choose_digit(num, len_left):
    sorted_list = list(dict.fromkeys([i for i in num]))
    sorted_list.sort()
    for digit in sorted_list:
        if len(num) - num.find(digit) >= len_left:
            if len_left > 1:
                return digit + choose_digit(num[num.find(digit)+1:], len_left - 1)
            else:
                return digit


def task_2(num, k):
    if check_num(num):
        return 'Incorrect num format!'
    if k > len(num):
        return 'It is impossible to cross out more digits than the number contains'
    len_left = len(num) - k
    return int(choose_digit(num, len_left))


def input_reading():
    num = str(input('Enter num: '))
    k = int(input('Enter k: '))
    return task_2(num, k)


if __name__ == '__main__':
    print(input_reading())
