# RUN task_1(date1, date2) to get result


days_in_months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

days_in_months_leap = days_in_months.copy()
days_in_months_leap[1] = days_in_months[1] + 1


def check_input_date(date_str):
    # check if the input string in INVALID format (then True)
    try:
        date_list = [int(i) for i in date_str.split('-')]
        if len(date_list) != 3 \
           or date_list[0] < 1000 or date_list[0] > 9999 \
           or date_list[1] < 1 or date_list[1] > 12 \
           or date_list[2] < 1 or date_list[2] > 31:
            raise ValueError('Incorrect date format!')
        if days_in_months_array(date_list[0])[date_list[1] - 1] < date_list[2]:
            raise ValueError('Incorrect date format!')
        return False
    except Exception:
        return True


def is_leap(year):
    # determines if a year is a leap year
    year = int(year)
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)


def days_in_year(year):
    if is_leap(year):
        return 366
    else:
        return 365


def days_in_months_array(year):
    if is_leap(year):
        return days_in_months_leap
    else:
        return days_in_months


def date_str_to_dict(date):
    # translates the date string into a dictionary,
    # with the month decremented by 1 to match the index in the days_in_months list
    return {
        'Y': int(date.split('-')[0]),
        'M': int(date.split('-')[1]) - 1,
        'D': int(date.split('-')[2]),
    }


def days_in_year_before_day(date_dict):
    # counts a number of days in this year before the date INCLUDING the current day
    days_array = days_in_months_array(date_dict['Y'])
    result = date_dict['D']
    for month in range(date_dict['M']):
        result += days_array[month]
    return result


def days_in_year_after_day(date_dict):
    # counts a number of days in this year after the date NOT INCLUDING the current day
    return days_in_year(date_dict['Y']) - days_in_year_before_day(date_dict)


def days_between(date1, date2):
    date1 = date_str_to_dict(date1)
    date2 = date_str_to_dict(date2)
    if date1['Y'] == date2['Y']:
        return days_in_year_before_day(date2) + days_in_year_after_day(date1) - days_in_year(date1['Y'])
    elif date1['Y'] < date2['Y']:
        return days_in_year_after_day(date1) + days_in_year_before_day(date2) \
                + sum([days_in_year(year) for year in range(date2['Y']+1, date1['Y'])])


def task_1(date1, date2):
    # reads input data and passes them to the days_between method in ascending order
    if check_input_date(date1):
        return 'Incorrect first date format!'
    if check_input_date(date2):
        return 'Incorrect second date format!'
    if date2 < date1:
        return days_between(date2, date1)
    else:
        return days_between(date1, date2)


