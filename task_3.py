import psycopg
from datetime import timedelta, datetime
import random

start_date = datetime(2023, 3, 15)


def db_credentials(db_name, user_name):
    return f'dbname={db_name} user={user_name}'


def random_date(day_range):
    result_date = start_date - timedelta(days=day_range) * random.random()
    return result_date.date(), result_date.month


def clear_tables(db_name, user_name):
    with psycopg.connect(db_credentials(db_name, user_name)) as conn:
        with conn.cursor() as cur:
            cur.execute("DELETE FROM accrual")
            cur.execute("DELETE FROM payment")


def create_tables(db_name, user_name, number_of_records, day_range):
    # creates tables in the existing database and fills it by random data
    with psycopg.connect(db_credentials(db_name, user_name)) as conn:
        with conn.cursor() as cur:
            cur.execute("""CREATE TABLE IF NOT EXISTS accrual(
               id serial PRIMARY KEY,
               date DATE,
               month INT)
            """)

            cur.execute("""CREATE TABLE IF NOT EXISTS payment(
               id serial PRIMARY KEY,
               date DATE,
               month INT)
            """)

            for i in range(number_of_records):
                date, month = random_date(day_range)
                cur.execute(
                    "INSERT INTO accrual (date, month) VALUES (%s, %s)",
                    (date, month))

            for i in range(number_of_records):
                date, month = random_date(day_range)
                cur.execute(
                    "INSERT INTO payment (date, month) VALUES (%s, %s)",
                    (date, month))


def get_tables_data(db_name, user_name):
    # get data from tables
    conn = psycopg.connect(db_credentials(db_name, user_name))
    cur = conn.cursor()
    cur.execute("SELECT * FROM accrual ORDER BY month, date")
    accruals = cur.fetchall()
    cur.execute("SELECT * FROM payment ORDER BY month, date")
    payments = cur.fetchall()
    return accruals, payments


def task_3(db_name, user_name):
    accruals, payments = get_tables_data(db_name, user_name)
    payments_accruals = {payment: None for payment in payments}
    lonely_payments = []

    # first priority
    for payment in payments:
        for index, accrual in enumerate(accruals):
            if accrual[2] == payment[2] and accrual[1] < payment[1]:
                payments_accruals[payment] = accruals.pop(index)
                break

    # sort accruals by date
    accruals.sort(key=lambda a: a[1])
    for payment in payments:
        if not payments_accruals[payment]:
            if accruals[0] < payment:
                payments_accruals[payment] = accruals.pop(0)
            else:
                del payments_accruals[payment]
                lonely_payments.append(payment)
    return payments_accruals, lonely_payments


if __name__ == '__main__':
    db_name = 'debts_db'
    user_name = 'postgres'
    # clear_tables(db_name, user_name)
    create_tables(db_name, user_name, 20, 365*5)
    accruals, payments = get_tables_data(db_name, user_name)
    print('\t', 'accruals', '\t\t\t', 'payments')
    for i in range(len(accruals)):
        print('\t', accruals[i][1], '\t', accruals[i][2], '\t', payments[i][1], '\t', payments[i][2])
    payments_accruals, lonely_payments = task_3(db_name, user_name)
    print('\n\npayments_accruals')
    for key, value in payments_accruals.items():
        print(key, ': ', value)
    print('\n\nlonely_payments', *lonely_payments)

